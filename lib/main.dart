import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  int _questionIndex = 0;
  int _totalScore = 0;
  final List<Map<String, Object>> _questions = const [
    {
      'questionText': 'What\'s IRAS?',
      'answers': [
        {'text':'Internal-Rotated-Adduction-Stance', 'score': 10},
        {'text':'International-Rotated-Addcution-Stance', 'score': 0},
        {'text':'Internal-Reloaded-Adduction-Stance', 'score': 0},
      ]
    },
    {
      'questionText': 'What\'s does \"Sei-Ping-Ma\" stands for?',
      'answers': [
        {'text':'Gewendeter Stand', 'score': 0},
        {'text':'IRAS', 'score': 10},
        {'text':'Langstock Stand', 'score': 0},
      ],
    },
    {
      'questionText': 'The meaning of "Gee-Ng-Ma" is',
      'answers': [
        {'text':'Stoßende Finger', 'score': 0},
        {'text':'Schockhand', 'score': 0},
        {'text':'IRAS', 'score': 10},
      ],
    },
  ];

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;

    setState(() {
      _questionIndex++;
    });
    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                selectHandler: _answerQuestion,
                questions: _questions,
                index: _questionIndex)
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
